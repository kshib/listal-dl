# listal-dl
Python script to download images from listal

## Requirements
Python 3

## Installation
```
git clone https://github.com/ksyko/listal-dl.git
cd listal-dl
pip install -r requirements.txt
```

## Usage
```
python dl.py https://www.listal.com/whatever/pictures
```
