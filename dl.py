import os

import requests
import sys
from lxml import html

page_link = sys.argv[1]
name = page_link.split('/')[3]


def get_images_from_page(p):
    print('Downloading Page ' + str(p))
    page = requests.get(page_link + '/' + str(p))
    tree = html.fromstring(page.content)
    image_links = tree.xpath("//div[@class='imagewrap-outer']//img/parent::a/@href")
    for image_link in image_links:
        if len(image_links) == 0:
            exit(0)
        page = requests.get(image_link)
        tree = html.fromstring(page.content)
        image = tree.xpath("//img[@class='pure-img']/@src")
        r = requests.get(str(image[0]))
        if not os.path.exists(name):
            os.mkdir(name)
        filename = image[0].split("/")[4]+ image[0].split("/")[-1]
        with open(name + '/' + filename, 'wb') as f:
            f.write(r.content)
            print('Downloaded ' + filename)
    p += 1
    get_images_from_page(p)


get_images_from_page(1)
